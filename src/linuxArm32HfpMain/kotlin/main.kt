package org.example.lvglHello

import io.gitlab.embedSoft.lvglKt.core.*
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDrawBuffer
import io.gitlab.embedSoft.lvglKt.core.display.DisplayDriver
import io.gitlab.embedSoft.lvglKt.core.event.LvglEvent
import io.gitlab.embedSoft.lvglKt.core.event.LvglEventCallback
import io.gitlab.embedSoft.lvglKt.core.lvglObject.*
import io.gitlab.embedSoft.lvglKt.core.styling.getPadRightStyle
import io.gitlab.embedSoft.lvglKt.core.styling.setBackgroundImageSourceStyle
import io.gitlab.embedSoft.lvglKt.core.styling.setRadiusStyle
import io.gitlab.embedSoft.lvglKt.drivers.FrameBuffer
import kotlinx.cinterop.memScoped
import lvDrivers.evdev_init
import lvgl.*
import kotlin.experimental.inv
import kotlin.system.exitProcess

private var currentHorRes = 800u
private var currentVertRes = 600u
private var useTouchScreen = false
private var enableDblBuffer = false
private var drawBuf: DisplayDrawBuffer? = null
private var displayDriver: DisplayDriver? = null

private var btnCounter = 1u

fun main(args: Array<String>) = memScoped {
    processArgs(args)
    printInfo()
    initSubSystems()
    setupDisplayDriver()
    if (!useTouchScreen) setupMouse() else setupTouchScreen()
    // TODO: Streamline GUI setup.
    lvExampleScroll3()
    runEventLoop()
}

private fun setupDisplayDriver() {
    drawBuf = DisplayDrawBuffer.create(currentHorRes * currentVertRes, enableDblBuffer)
    displayDriver = FrameBuffer.createDisplayDriver(drawBuf!!) {
        horRes = currentHorRes.toShort()
        vertRes = currentVertRes.toShort()
        register()
    }
}

private fun printInfo() {
    println("Screen Resolution: ${currentHorRes}x$currentVertRes")
    println("Using Touchscreen: $useTouchScreen")
    println("Double Buffering: $enableDblBuffer")
}

private fun initSubSystems() {
    initLvgl()
    FrameBuffer.open()
    // Initialize input devices.
    evdev_init()
}

private fun processArgs(args: Array<String>) {
    if ("-h" in args) {
        printProgramUsage()
        exitProcess(0)
    }
    useTouchScreen = "--useTouchScreen" in args
    enableDblBuffer = "--doubleBuffer" in args
    processScreenResolutionArgs(args)
}

private fun printProgramUsage() {
    println(
        """
            Program Usage
            =============
            
            lvgl_hello
            lvgl_hello --horRes=<num> --vertRes=<num>
            lvgl_hello --useTouchScreen
            lvgl_hello --horRes=<num> --vertRes=<num> --useTouchScreen
            
            Set the screen resolution:
              lvgl_hello --horRes=1280 --vertRes=720
            
            Use the Touchscreen instead of the Mouse as a pointer device:
              lvgl_hello --useTouchScreen
        """.trimIndent()
    )
}

private fun processScreenResolutionArgs(args: Array<String>) {
    var tmpHor = 0u
    var tmpVert = 0u
    args.forEach { a ->
        if (a.startsWith("--horRes=")) tmpHor = extractHorizontalResolution(a)
        else if (a.startsWith("--vertRes=")) tmpVert = extractVerticalResolution(a)
    }
    if (tmpHor > 0u && tmpVert > 0u) {
        currentHorRes = tmpHor
        currentVertRes = tmpVert
    }
}

private fun extractHorizontalResolution(arg: String) = try {
    arg.trim().replace("--horRes=", "").toUInt()
} catch (ex: NumberFormatException) {
    0u
}

private fun extractVerticalResolution(arg: String) = try {
    arg.trim().replace("--vertRes=", "").toUInt()
} catch (ex: NumberFormatException) {
    0u
}

private fun floatBtnHandler(evt: LvglEvent, userData: Any?) {
    val floatBtn = evt.target
    if (evt.code == LV_EVENT_CLICKED) {
        val list = userData as LvglObject
        val listBtn = lv_list_add_btn(list = list.lvObjPtr, icon = LV_SYMBOL_AUDIO, txt = "Track $btnCounter")
            .toLvglObject()
        btnCounter++
        lv_obj_move_foreground(floatBtn.lvObjPtr)
        listBtn.scrollToView(lv_anim_enable_t.LV_ANIM_ON)
    }
}

private fun lvExampleScroll3() {
    val list = lv_list_create(Screen.activeScreen.lvObjPtr).toLvglObject()
    val callback = LvglEventCallback.create(::floatBtnHandler, list)
    list.setSize(width = 280, height = 220)
    list.center()
    while (btnCounter <= 2u) {
        lv_list_add_btn(list = list.lvObjPtr, icon = LV_SYMBOL_AUDIO, txt = "Track $btnCounter")
        btnCounter++
    }
    lv_btn_create(list.lvObjPtr).toLvglObject().apply {
        setSize(width = 50, height = 50)
        addFlag(LV_OBJ_FLAG_FLOATING)
        align(align = LvglAlignment.BOTTOM_RIGHT.value.toUByte(), xOffset = 0,
            yOffset = getPadRightStyle(LvglPart.MAIN.value).inv())
        addEventCallback(LV_EVENT_ALL, callback)
        setRadiusStyle(LV_RADIUS_CIRCLE.toShort())
        setBackgroundImageSourceStyle(LV_SYMBOL_PLUS)
        lv_obj_set_style_text_font(obj = lvObjPtr, value = lv_theme_get_font_large(lvObjPtr), selector = 0)
    }
}
