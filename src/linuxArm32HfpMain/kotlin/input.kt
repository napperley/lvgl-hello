package org.example.lvglHello

import io.gitlab.embedSoft.lvglKt.core.Image
import io.gitlab.embedSoft.lvglKt.core.Screen
import io.gitlab.embedSoft.lvglKt.drivers.input.InputDeviceDriver
import lvgl.LV_SYMBOL_GPS
import lvgl.lv_indev_type_t

internal fun setupMouse() {
    // Add the mouse as input device. Use the driver which reads the PC's mouse.
    val driver = InputDeviceDriver.create().apply { type = lv_indev_type_t.LV_INDEV_TYPE_POINTER }
    val dev = driver.register()
    // Create an image object for the cursor.
    val cursor = Image.create(Screen.activeScreen)
    cursor.setSrc(LV_SYMBOL_GPS)
    dev.setCursor(cursor)
}

internal fun setupTouchScreen() {
    // Add the Touchscreen as input device. Use the driver which reads the Touchscreen.
    InputDeviceDriver.create().apply {
        type = lv_indev_type_t.LV_INDEV_TYPE_POINTER
        register()
    }
}
