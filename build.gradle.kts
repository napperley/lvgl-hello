group = "org.example"
version = "0.1"

plugins {
    kotlin("multiplatform") version "1.6.0"
}

repositories {
    mavenCentral()
    mavenLocal()
}

kotlin {
    linuxArm32Hfp {
        compilations.getByName("main") {
            dependencies {
                val lvglktVer = "0.1.0"
                implementation("io.gitlab.embed-soft:lvglkt-core:$lvglktVer")
                implementation("io.gitlab.embed-soft:lvglkt-drivers:$lvglktVer")
            }
            cinterops.create("lvgl") {
                val homeDir = System.getenv("HOME") ?: ""
                val lvglDir = "$homeDir/lvgl/lvgl-8.0.2"
                val lvglDriversDir = "$homeDir/lv_drivers"
                includeDirs(
                    lvglDir,
                    "$lvglDriversDir/lv_drivers-6.1.1/display",
                    "$lvglDriversDir/lv_drivers-6.1.1/indev",
                    lvglDriversDir
                )
                compilerOpts(
                    "-DLV_LVGL_H_INCLUDE_SIMPLE",
                    "-DUSE_EVDEV=1"
                )
            }
        }
        binaries {
            executable("lvgl_hello") {
                entryPoint = "org.example.lvglHello.main"
            }
        }
    }
}